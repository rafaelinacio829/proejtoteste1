#include <stdio.h>

int main(void)

{
//DECLAÇÃO DE VARIÁVEIS 
int e,h,cg, mp;

//ENTRADA DE DADOS

printf("\nInforme a pontuacao em Exatas: ");
scanf("%d", &e);
printf("\nInforme a pontuacao em Humanas: ");
scanf("%d", &h);
printf("\nInforme a pontuacao em Conhecimentos Gerais: ");
scanf("%d", &cg);
//PROCESSAMENTO ;
mp = ((e*3) + (h*2)*cg)/6;

if (mp <= 250)
{   
    printf("\nMedia do candidato = %d pontos " ,mp);
    printf("\nConceito: INSUFICIENTE");
    
}
else
{
    if (mp >=251 && mp <=500)
    {
    printf("\nMedia do candidato = %d pontos " ,mp);
    printf("\nConceito: BAIXO");
    
    }
    else
    {
        if (mp >=501 && mp <=700)
        {
            printf("\nMedia do candidato = %d pontos " ,mp);
            printf("\nConceito: REGULAR");
            
        }
        else
        {
            if (mp >=701 && mp <=900)
            {
                printf("\nMedia do candidato = %d pontos " ,mp);
                printf("\nConceito: BOM");

            }
            else
            {
                if ( mp > 900)
                {
                    printf("\nMedia do candidato = %d pontos", mp);
                    printf("\nConceito: EXCELENTE");
                }
            }
            
        }
        
    }
 
}

return 0;
}
